#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <time.h>
#include <omp.h>
#include <mpi.h>
#include <unistd.h>
#ifdef MKL_SUPPORT
#include <mkl.h>
#endif

struct eMatrix
{
    double *a;
    int n;
    int *row_permut;
    eMatrix(int nn)
    : n(nn)
    {
        row_permut = new int [n];
    }
    void reset_permut()
    {
      for (int i = 0; i < n; i++) 
        row_permut[i] = i;
    }
    ~eMatrix()
    {
        delete [] row_permut;
    }
    inline void swap_rows(int i, int j)
    {
        double bx;
        for (int k = 0; k < n; k++) {
            bx = (*this)(i, k);
            (*this)(i, k) = (*this)(j, k);
            (*this)(j, k) = bx;
        }
        int y = row_permut[i];
        row_permut[i] = row_permut[j];
        row_permut[j] = y;
    }
    inline double & operator()(int i, int j) const
    {
        return a[j+i*n];
    }
    void parallel_lup(double *X, double *Y, int L3_sz,int tnum);
};

void eMatrix::parallel_lup(double *X, double *Y, int L3_sz, int tnum)
{
    double dmax[tnum];
    int imax[tnum];
    eMatrix &lu=*this;
    int bs;
    double max;
    int max_row;
    int N = n;
    bs = N*N - L3_sz > 0 ? N - (int)sqrt(N*N-L3_sz) : N;
    for (int i = 0;;) {
        //
        // |_A_|_Y_|
        // | X | D |
        //
        //prepare well-aligned X
        for (int z = i + bs; z < n; z++) {
            for (int g = i; g < i+bs; g++)
                X[g - i + bs*(z-i-bs)] = lu(z, g);
        }
        // factorize A 
        for (int ii = i; ii < i+bs; ii++) {
        ////
        double coeff_ii;
        #pragma omp parallel shared(coeff_ii,X,lu,ii,i,bs)
        {////parallel
        int id = omp_get_thread_num();
        dmax[id] = 0;
        #pragma omp parallel for
        for (int k = ii; k < i+bs; k++) {
            if (fabs(lu(k,ii)) > dmax[id]) {
                dmax[id] = fabs(lu(k,ii));
                imax[id] = k;
            }
        }
        #pragma omp barrier
        #pragma omp single
        {//single
        max = 0;
        for (int f = 0; f < tnum; f++) {
            if (max < dmax[f]) {
                max = dmax[f];
                max_row = imax[f];
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
        }
        if (ii != max_row)
            swap_rows(ii, max_row);

        coeff_ii = lu(ii,ii);
        }//end_single
        
        #pragma omp for //schedule(dynamic,1)
        for (int j = ii + 1; j < i+bs; j++) {
            double coef = (lu(j, ii) /= coeff_ii);
            for (int k = ii + 1; k < n; k++)
                lu(j, k) -= coef * lu(ii, k);
        }
        #pragma omp for
        for (int j = 0; j < n-i-bs; j++) {
            double coef = (X[j*bs + ii - i] /= coeff_ii);
            for (int k = ii-i+1; k < bs; k++)
                X[j*bs + k] -= coef * lu(ii, k+i);
        }


        }////end_parallel
        ////
        }
        
        //now D -= X*Y
        
        //prepare Y

        int gg = n-i-bs;
        if (gg == 0) break;
        for (int z = 0; z < bs; z++) {
            for (int g = 0; g < gg; g++)
                Y[z + g*bs] = lu(i+z, i+bs+g);
        }
        int bdd = 32;// bdd = bs is a bit slower
        for (int a = 0; a < gg; a+=bdd) {
            int at = a+bdd;
            if (at > gg) at = gg;
            
            for (int b = 0; b < gg; b+=bdd) {
                int bt = b+bdd;
                if (bt > gg) bt = gg;

                #pragma omp parallel for shared(Y,X,lu,i,bs,bt,at) //collapse(2) //schedule(dynamic,1) 
                for (int j = a; j < at; j++) {
                    for (int k = b; k < bt; k++) {
                        double xx = 0;
                        for (int m = 0; m < bs; m++)
                          xx += X[m + j*bs] * Y[m + bs*k];
                        lu(j+i+bs, k+i+bs)-= xx;
                    }
                }
            }
        }

        for (int z = i + bs; z < n; z++) {
            for (int g = i; g < i+bs; g++)
                lu(z,g) = X[g - i + bs*(z-i-bs)];
        }
        i+=bs;
        if (i == n) break;
        N-=bs;
        bs = N*N - L3_sz > 0 ? N - (int)sqrt(N*N-L3_sz) : N;
        
    }
}


struct Matrix
{
    double *a;
    int m, n;
    int *row_permut, *col_permut;
    Matrix(const Matrix& z)
    : m(z.m), n(z.n)
    {
        a = new double [m*n];
        row_permut = new int [m];
        col_permut = new int [n];
        for (int i = 0; i < m; i++) 
            row_permut[i] = z.row_permut[i];
        for (int i = 0; i < n; i++)
            col_permut[i] = z.col_permut[i];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                (*this)(i,j) = z(i,j);
        }
    }
    void zero()
    {
      for (int i = 0; i < m; i++) {
       for (int j = 0; j < n; j++)
        (*this)(i,j) = 0;
      }
    }
    void save_permutation(int *perm_info, int block_s, int order)
    {
        for (int i = 0; i < block_s; i++)
           row_permut[i+order*block_s] = perm_info[i] + order*block_s;

        int ss = order*block_s;
        if (ss > 0) {
           double *buf = new double [block_s*ss];
           for (int i = 0; i < block_s; i++) {
             if (i != perm_info[i]) {
               for (int j = 0; j < ss; j++) {
                 buf[j+i*ss] = (*this)(i+ss,j);
               }
             }
           }
           for (int i = 0; i < block_s; i++) {
             if (i != perm_info[i]) {
               for (int j = 0; j < ss; j++) {
                 (*this)(i+ss, j) = buf[j+perm_info[i]*ss];
               }
             }
           }
           delete [] buf;
        }
    }
    void place_block(double *mb, int block_s, int order)
    {
        int col_s, row_s;
        int nn = n/block_s;
        row_s = block_s*(order/nn);
        col_s = block_s*(order%nn);
        for (int i = 0; i < block_s; i++) {
          for (int j = 0; j < block_s; j++)
            a[col_s + j + n*(row_s + i)]= mb[j+block_s*i];
        }
    }
    /*void split(std::vector<double*> & v, int nb)
    {
      int sz = n/nb;
      for (int i = 0; i < nb; i++) {
        for (int j = 0; j < nb; j++) {
          double *p = new double [sz*sz];
          v.push_back(p);
          for (int k = 0; k < sz; k++) {
            for (int s = 0; s < sz; s++)
              p[k*sz + s] = (*this)(k + i*sz, s + j*sz);
          }
        }
      }
    }*/
    double frobenius() const
    {
        double res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res += (*this)(i,j) * (*this)(i,j);
        }
        return sqrt(res);
    }
    bool operator==(const Matrix& x) const
    {
        if (m != x.m || n != x.n)
            return false;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if ((float)(*this)(i, j) != (float)x(i, j)) {
                    printf("%lf %lf\n", (*this)(i, j), x(i, j));
                    return false;
                }
            }
        }
        return true;
    }
    Matrix operator+(const Matrix& b) const
    {
        const Matrix &a = *this;
        Matrix res(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res(i, j) = a(i, j) + b(i, j);
        }
        return res;
    }
    Matrix operator-(const Matrix& b) const
    {
        const Matrix &a = *this;
        Matrix res(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res(i, j) = a(i, j) - b(i, j);
        }
        return res;
    }
    Matrix operator*(const Matrix& b) const
    {
        const Matrix &a = *this;
        Matrix res(m, b.n);
        for (int i = 0; i < res.m; i++) {
            for (int j = 0; j < res.n; j++) {
                res(i, j) = a(i, 0) * b(0, j);
                for (int k = 1; k < n; k++)
                    res(i, j) += a(i, k) * b(k, j);
            }
        }
        return res;
    }
    Matrix(int mm, int nn = 0)
    : m(mm), n(nn)
    {
        if (nn == 0)
            n = m;
        a = new double [m*n];
        row_permut = new int [m];
        col_permut = new int [n];
        for (int i = 0; i < m; i++) 
            row_permut[i] = i;
        for (int i = 0; i < n; i++)
            col_permut[i] = i;
    }

    Matrix() {}

    ~Matrix()
    {
        delete [] a;
        delete [] row_permut;
        delete [] col_permut;
    }
    void print() const
    {
        for (int i = 0; i < m; i++) {
            printf("| ");
            for (int j = 0; j < n; j++)
                printf("%lf ", (*this)(i, j));
            printf("|\n");
        }
    }
    void random()
    {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                (*this)(i, j) = (double) rand() / RAND_MAX;
        }
    }
    inline void swap_rows(int i, int j)
    {
        double b;
        for (int k = 0; k < n; k++) {
            b = (*this)(i, k);
            (*this)(i, k) = (*this)(j, k);
            (*this)(j, k) = b;
        }
        int y = row_permut[i];
        row_permut[i] = row_permut[j];
        row_permut[j] = y;
    }
    /*inline void swap_rows_fast(int i, int j)
    {
        double b;
        for (int k = 0; k < n; k++) {
            b = (*this)(i, k);
            (*this)(i, k) = (*this)(j, k);
            (*this)(j, k) = b;
        }
    }*/
    inline double & operator()(int i, int j) const
    {
        return a[j+i*n];
    }
    void parallel_lup(double *X, double *Y, int L3_sz,int tnum);
    void slow_parallel_lup();
    void sequential_lup();
    void unlup();
};

void Matrix::unlup()
{
    Matrix& lu = *this;
    for (int i = n-1; i >= 0; i--) {
        for (int j = 0; j < i; j++)
            lu(i, i) += lu(i, j) * lu(j, i);
        for (int j = i-1; j >= 0; j--) {
            lu(i, j) *= lu(j, j);
            for (int k = 0; k < j; k++)
                lu(i, j) += lu(i, k) * lu(k, j);
        }
        for (int j = i-1; j >= 0; j--) {
            for (int k = 0; k < j; k++)
                lu(j, i) += lu(j, k) * lu(k, i);
        }
    }
    for (int i = 0; i < n; i++) {
        while (row_permut[i] != i)
            swap_rows(i, row_permut[i]);
    }
}

void Matrix::sequential_lup()
{
    if (m != n) {
        printf("Non-square matrix can't be decomposed\n");
        return;
    }
    Matrix &lu = *this;
    int max_row = 0;
    for (int i = 0; i < n; i++) {
        double max = 0;
        for (int k = i; k < n; k++) {
            if (fabs(lu(k, i)) > max) {
                max = fabs(lu(k, i));
                max_row = k;
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
            return;
        }
        if (i != max_row)
            swap_rows(i, max_row);

        for (int j = i + 1; j < n; j++) {
            lu(j, i) /= lu(i, i);
            for (int k = i + 1; k < n; k++)
                lu(j, k) -= lu(j, i) * lu(i, k);
        }
    }
}


void Matrix::parallel_lup(double *X, double *Y, int L3_sz, int tnum)
{
    double dmax[4];
    int imax[4];
    Matrix &lu=*this;
    int bs;
    double max;
    int max_row;
    int N = n;
    bs = N*N - L3_sz > 0 ? N - (int)sqrt(N*N-L3_sz) : N;
    for (int i = 0;;) {
        //
        // |_A_|_Y_|
        // | X | D |
        //
        //prepare well-aligned X
        for (int z = i + bs; z < n; z++) {
            for (int g = i; g < i+bs; g++)
                X[g - i + bs*(z-i-bs)] = lu(z, g);
        }
        // factorize A 
        for (int ii = i; ii < i+bs; ii++) {
        ////
        double coeff_ii;
        #pragma omp parallel shared(coeff_ii,X,lu,ii,i,bs)
        {////parallel
        int id = omp_get_thread_num();
        dmax[id] = 0;
        #pragma omp parallel for
        for (int k = ii; k < i+bs; k++) {
            if (fabs(lu(k,ii)) > dmax[id]) {
                dmax[id] = fabs(lu(k,ii));
                imax[id] = k;
            }
        }
        #pragma omp barrier
        #pragma omp single
        {//single
        max = 0;
        for (int f = 0; f < tnum; f++) {
            if (max < dmax[f]) {
                max = dmax[f];
                max_row = imax[f];
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
        }
        if (ii != max_row)
            swap_rows(ii, max_row);
        coeff_ii = lu(ii,ii);
        }//end_single
        
        #pragma omp for //schedule(dynamic,1)
        for (int j = ii + 1; j < i+bs; j++) {
            double coef = (lu(j, ii) /= coeff_ii);
            for (int k = ii + 1; k < n; k++)
                lu(j, k) -= coef * lu(ii, k);
        }
        #pragma omp for
        for (int j = 0; j < n-i-bs; j++) {
            double coef = (X[j*bs + ii - i] /= coeff_ii);
            for (int k = ii-i+1; k < bs; k++)
                X[j*bs + k] -= coef * lu(ii, k+i);
        }


        }////end_parallel
        ////
        }
        
        //now D -= X*Y
        
        //prepare Y

        int gg = n-i-bs;
        if (gg == 0) break;
        for (int z = 0; z < bs; z++) {
            for (int g = 0; g < gg; g++)
                Y[z + g*bs] = lu(i+z, i+bs+g);
        }
        int bdd = 32;// bdd = bs is a bit slower
        for (int a = 0; a < gg; a+=bdd) {
            int at = a+bdd;
            if (at > gg) at = gg;
            
            for (int b = 0; b < gg; b+=bdd) {
                int bt = b+bdd;
                if (bt > gg) bt = gg;

                #pragma omp parallel for shared(Y,X,lu,i,bs,bt,at) //collapse(2) //schedule(dynamic,1) 
                for (int j = a; j < at; j++) {
                    for (int k = b; k < bt; k++) {
                        double xx = 0;
                        for (int m = 0; m < bs; m++)
                          xx += X[m + j*bs] * Y[m + bs*k];
                        lu(j+i+bs, k+i+bs)-= xx;
                    }
                }
            }
        }

        for (int z = i + bs; z < n; z++) {
            for (int g = i; g < i+bs; g++)
                lu(z,g) = X[g - i + bs*(z-i-bs)];
        }
        i+=bs;
        if (i == n) break;
        N-=bs;
        bs = N*N - L3_sz > 0 ? N - (int)sqrt(N*N-L3_sz) : N;
        
    }
}

void Matrix::slow_parallel_lup()
{
    if (m != n) {
        printf("Non-square matrix can't be decomposed\n");
        return;
    }
    Matrix &lu = *this;
    int max_row = 0;
    for (int i = 0; i < n; i++) {
        double max = 0;
        for (int k = i; k < n; k++) {
            if (fabs(lu(k, i)) > max) {
                max = fabs(lu(k, i));
                max_row = k;
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
            return;
        }
        if (i != max_row)
            swap_rows(i, max_row);
        #pragma omp parallel for shared (lu, i) //schedule(dynamic,1)
        for (int j = i + 1; j < n; j++) {
            lu(j, i) /= lu(i, i);
            for (int k = i + 1; k < n; k++)
                lu(j, k) -= lu(j,i) * lu(i, k);
        }
    }
}

void random(double *x, int m, int n)
{
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++)
         x[j+i*m] = (double) rand() / RAND_MAX;
    }
}

enum {numof_tests = 1, arr_sz = 1, b = 64};
const int mtrx[arr_sz] = {256};//, 4096, 8192};
const int L3_sz = 524288; //6Mib approx 2^19 doubles, 699050 for 8Mib
int size_mpi, rank_mpi, sqrt_size_mpi;

#ifdef MKL_SUPPORT
void update(double *B, double *A, double *C)
{
 cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,b,b,b,-1,
    A,b,C,b,1,B,b);
}
#else
void update(double *B, double *A, double *C)
{//perform B-=A*C
   if (!B || !A || !C) printf("ERROR: %d: %d %d %d\n", rank_mpi, !B, !A, !C);
   for (int i = 0; i < b; i++) {
     for (int j = 0; j < b; j++) {
       for (int k = 0; k < b; k++) {
          B[j+i*b] -= A[k+i*b]*C[j+k*b];
       }
     }
   }
}
#endif
void update_down(int i, int table_len, int *table,
  double** blocks, double *dd)
{
    for (int ee = i+1; ee < table_len; ee++) {
      int index = i+ee*table_len;
      if (rank_mpi == table[index]) {
        //#pragma omp parallel for
        for (int j = 0; j < b; j++) {
          for (int m = 0; m < b; m++) {
            double coef = (blocks[index][j+m*b] /= dd[j+j*b]);
            for (int k = j+1; k < b; k++) {
              blocks[index][k+m*b] -= coef*dd[k+j*b];
            }
          }
        }
        if (!blocks[index]) printf("EWWWWWWW\n");
        MPI_Send(blocks[index], b*b, MPI_DOUBLE, 0, index,
                 MPI_COMM_WORLD);
      }
    }
}
void update_right(int i, int table_len, int* table,
  double**  blocks, int *perm, double *rr)
{
    double *buf = new double [b*b];
    for (int ee = i+1; ee < table_len; ee++) {
      int index = ee+i*table_len;
      if (rank_mpi == table[index]) {
        for (int k = 0; k < b; k++) {
          int ii = perm[k];
          if (ii != k) {//swap
            for (int q = 0; q < b; q++) {
              buf[q+k*b] = blocks[index][q+k*b];
            } 
          }
         }
        for (int k = 0; k < b; k++) {
          int ii = perm[k];
          if (ii != k) {
            for (int q = 0; q < b; q++)
              blocks[index][q+k*b] = buf[q+ii*b];
          }
         }
        for (int k = 0; k < b; k++) {
          //#pragma omp parallel for
          for (int j = k+1; j < b; j++) {
             double coef = rr[k+j*b];
             for (int m = 0; m < b; m++) 
               blocks[index][m+j*b] -= coef*blocks[index][m+k*b];
          }
        }
        MPI_Send(blocks[index], b*b, MPI_DOUBLE, 0, index,
         MPI_COMM_WORLD);

      }
    }
    delete [] buf;
}
void slave()
{
   // int true_perm[b];
    eMatrix xm(b);
    //int perm[b]; double rr[b*b];
    int *perm = new int [b];
    double *rr = new double [b*b];
    //double dd[b*b], left_block[b*b], up_block[b*b], *lbp, *ubp;
    double *dd = new double [b*b];
    double *left_block = new double [b*b];
    double *up_block = new double [b*b];
    double *lbp, *ubp;
    int tnum = omp_get_max_threads();
    double *X = new double[L3_sz];
    double *Y = new double[L3_sz];
    int table_max_sz = mtrx[arr_sz-1]/b;
    int table[table_max_sz*table_max_sz];//table of blocks
    int table_len;
  while (1) {
    MPI_Bcast(&table_len, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (table_len) {//if there any job
      MPI_Bcast(&table, table_len*table_len, MPI_INT, 0, MPI_COMM_WORLD);
      //print table for debug
      for (int i = 0; i < table_len && rank_mpi == 1; i++) {
        for (int j = 0; j < table_len; j++)
          printf("%d ", table[j+table_len*i]);
        printf("\n");
      }
      //now slave should generate blocks and send them to the master
      double* blocks[table_len*table_len];
      for (int i = 0; i < table_len; i++) {
        for (int j = 0; j < table_len; j++) {
          int tag = j+table_len*i;
          if (table[tag] == rank_mpi) {
            double *x = new double [b*b];
            random(x, b, b);
            blocks[tag] = x;
            MPI_Send(x, b*b, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
            
          } else {
            blocks[tag] = 0;
          }
        }
      }
    
      MPI_Barrier(MPI_COMM_WORLD);//wait master
      
      for (int i = 0; i < table_len; i++) {
        int cur_index = i+i*table_len;
        int cur_node = table[cur_index];
        if (cur_node == rank_mpi) {
          //this slave has current upper-left block
          xm.reset_permut();
          xm.a = blocks[cur_index];
          xm.parallel_lup(X, Y, L3_sz, tnum);
         // printf("cur_node: %d factorized\n", rank_mpi);
        }
        //sleep(2);
        //now update blocks to the right
        for (int ind_col = i+1; ind_col < table_len; ind_col++) {
          int node = table[ind_col+i*table_len];
          if (node == cur_node)
            break;
          if (rank_mpi == cur_node) {
            if (!xm.row_permut) printf("AAAAAA\n");
            MPI_Send(xm.row_permut, b, MPI_INT, node,
                 table_len*table_len+i, MPI_COMM_WORLD);
            if (!xm.a) printf("BBBBBB\n");
            MPI_Send(xm.a, b*b, MPI_DOUBLE, node, cur_index, MPI_COMM_WORLD);
          }
          if (rank_mpi == node) {
            MPI_Recv(perm, b, MPI_INT,cur_node,table_len*table_len+i,
              MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(rr, b*b, MPI_DOUBLE, cur_node, cur_index, MPI_COMM_WORLD,
              MPI_STATUS_IGNORE);
            update_right(i, table_len, table, blocks, perm, rr); 
            break;
          }
        }
        if (cur_node == rank_mpi) {
         update_right(i, table_len, table, blocks, xm.row_permut, xm.a);
         MPI_Send(xm.row_permut, b, MPI_INT, 0, table_len*table_len+i, MPI_COMM_WORLD);
        }
        //update blocks to the bottom
        //sleep(5); 
        for (int ind_row = i+1; ind_row < table_len; ind_row++) {
           int node = table[i+ind_row*table_len]; 
           if (node == cur_node)
             break;
           if (rank_mpi == cur_node) {
             if (!xm.a) printf("CCCCCCC\n");
             MPI_Send(xm.a, b*b, MPI_DOUBLE, node, cur_index, MPI_COMM_WORLD);
           }
           if (rank_mpi == node) {
             MPI_Recv(dd, b*b, MPI_DOUBLE, cur_node, cur_index,MPI_COMM_WORLD,
                MPI_STATUS_IGNORE);
             update_down(i, table_len, table, blocks, dd);
             break;
           }
        }
        if (cur_node == rank_mpi) {
          update_down(i, table_len, table, blocks, xm.a);
          MPI_Send(xm.a, b*b, MPI_DOUBLE, 0, cur_index, MPI_COMM_WORLD);
        }
        //now D-=XY
        //ubp = lbp = 0;
        for (int w = i+1; w < table_len; w++) {
          int left_index = i+w*table_len;
          int left_owner = table[left_index];
          if (rank_mpi == left_owner)
            lbp = blocks[left_index];
          else
            lbp = left_block;
          for (int vv = i+1; vv < table_len; vv++) {
               if (table[vv+w*table_len] == left_owner)
                   break;
               if (rank_mpi == left_owner) {
                 if (!blocks[left_index]) printf("DDDDDD\n");
                 MPI_Send(blocks[left_index], b*b, MPI_DOUBLE,
                   table[vv+w*table_len],left_index, MPI_COMM_WORLD);
               } else if (rank_mpi == table[vv+w*table_len]){
                 MPI_Recv(lbp, b*b, MPI_DOUBLE, left_owner,left_index,
                   MPI_COMM_WORLD, MPI_STATUS_IGNORE);
               }
          }
          for (int q = i+1; q < table_len; q++) {
            int up_index = q+i*table_len;
            int up_owner = table[up_index];
            if (rank_mpi == up_owner)
              ubp = blocks[up_index];
            else
              ubp = up_block;
            int o_index = q+w*table_len;
            int cur_owner = table[o_index];
           
            /*if ((rank_mpi == up_owner || rank_mpi == cur_owner)
              && cur_owner != up_owner)
            {
               printf("EEEEEEEEE\n");
               MPI_Sendrecv(blocks[up_index], b*b, MPI_DOUBLE,
                cur_owner, 0, ubp, b*b, MPI_DOUBLE,
                up_owner, MPI_ANY_TAG, MPI_COMM_WORLD,
                MPI_STATUS_IGNORE);
            }*/
            if (rank_mpi == up_owner && cur_owner != up_owner)
               MPI_Send(blocks[up_index], b*b, MPI_DOUBLE,
                  cur_owner, up_index, MPI_COMM_WORLD);
            else if (rank_mpi == cur_owner && cur_owner != up_owner)
               MPI_Recv(ubp, b*b, MPI_DOUBLE, up_owner, up_index,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if (rank_mpi == cur_owner)
              update(blocks[o_index], lbp, ubp);
          }
        }
      }

      //free resources
      for (int g = 0; g < table_len*table_len; g++)
          if (blocks[g]) delete [] blocks[g]; 
      
    } else {//no job
      delete [] Y;
      delete [] X;
      delete [] perm;
      delete [] rr;
      delete [] dd;
      delete [] left_block;
      delete [] up_block;
      break;
    }
  }
}

void master()
{
    MPI_Status status;
    int table_max_sz = mtrx[arr_sz-1]/b;
    int table[table_max_sz*table_max_sz];
    int table_len;
    double res[arr_sz];
    double buf, error; //relative error based on frobenius norm
    int tnum;
    //double mb[b*b];
    double *mb = new double [b*b];
    //int perm_info[b];
    int *perm_info = new int [b];
    //double *X = new double[L3_sz];
    //double *Y = new double[L3_sz];
   
    ///////////
    printf("%d threads on master\n", (tnum = omp_get_max_threads()));
    printf("%d nodes available, sqrt = %d\n", size_mpi,sqrt_size_mpi);
    for (int i = 0; i < arr_sz; i++) {
        //set block-cycle table
        table_len = mtrx[i]/b;
        for (int z = 0; z < table_len; z++) {
          for (int v = 0; v < table_len; v++) {
            table[v+z*table_len] = 1 + sqrt_size_mpi*(z % sqrt_size_mpi) + v%sqrt_size_mpi;
          }
        }
        //table ready
        buf = 0;
        error = 0;
        for (int k = 0 ; k < numof_tests; k++) {
            Matrix A(mtrx[i]), LUP(mtrx[i]);
            MPI_Bcast(&table_len, 1, MPI_INT, 0, MPI_COMM_WORLD);
            int tls = table_len*table_len;
            MPI_Bcast(&table, tls, MPI_INT, 0, MPI_COMM_WORLD);
            //now slaves generates blocks. master should collect them.
            for (int i = 0; i < tls; i++) {
                MPI_Recv(mb, b*b, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                  MPI_COMM_WORLD, &status);
                A.place_block(mb, b, status.MPI_TAG);
                /*printf("Printing %d:\n", status.MPI_TAG);
                for (int v = 0; v < b; v++) {
                 for (int k = 0; k < b; k++)
                   printf("%lf ",mb[k+v*b]);
                 printf("\n");
                }*/
            }
           // printf("matrix:\n");
           // A.print();
            MPI_Barrier(MPI_COMM_WORLD);//wait slaves
            printf("OKEY!\n");
            //LUP.zero();
            double t = MPI_Wtime();
            //Magic here...
            int perm_count = table_len, bl_count = tls; 
            while (perm_count || bl_count) {
              MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
              if (status.MPI_TAG >= tls) {
              //received permutation info
                MPI_Recv(perm_info, b, MPI_INT, status.MPI_SOURCE, status.MPI_TAG,
                   MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                LUP.save_permutation(perm_info, b, status.MPI_TAG-tls);//should also permute left blocks!
                perm_count--;
                printf("RECEIVED PERMUT: %d\n", status.MPI_TAG-tls);
                //for (int v = 0; v < b; v++) printf("%d ", perm_info[v]);
                //printf("\n\n");
              } else {
              //received block
                MPI_Recv(mb, b*b, MPI_DOUBLE, status.MPI_SOURCE, status.MPI_TAG,
                   MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                LUP.place_block(mb, b, status.MPI_TAG);
                bl_count--;
                printf("RECEIVED BLOCK: %d\n", status.MPI_TAG);
                if (status.MPI_TAG == tls-1)//the last block received
                  break;
              }
              //printf("Printing: \n\n"); LUP.print(); printf("\n\n");
            }
            
            //printf("final print: \n\n");LUP.print(); printf("\n\n");
            //A.parallel_lup(X, Y,L3_sz, tnum);
            //printf("AN:\n\n"); A.print(); printf("\n\n");
            //End of magic...
            t = MPI_Wtime() - t;
            buf += t;
            if (perm_count || bl_count) printf("!!!!!!!!!!!!!!!!!!!\n");

            //counting error
            LUP.unlup();
            //LUP.print();
            //printf("\n\n");
            error += (A - LUP).frobenius() / A.frobenius();
        }
        res[i] = buf / numof_tests;
        error /= numof_tests;
        printf("%d | err: %e | %lf sec \n",
                mtrx[i], error, res[i]);
    }
    printf("Complete. Calculating exponent...\n");
    for (int i = 0; i < arr_sz - 1; i++) {
        printf("%d - %d : %lf\n", mtrx[i], mtrx[i+1],
        log2( (double)res[i+1]/res[i] ));
    }
    //////////////
    table_len = 0; //set the end of job
    MPI_Bcast(&table_len, 1, MPI_INT, 0, MPI_COMM_WORLD);
    //free resources
    //delete [] X; delete [] Y;
    delete [] perm_info; delete [] mb;
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size_mpi);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_mpi);
    srand(time(0)+100*rank_mpi);
    sqrt_size_mpi = sqrt(size_mpi-1);
    if (rank_mpi == 0)
      master();
    else
      slave();
    MPI_Finalize();
    return 0;
}

//TODO
//1. Move from send-recv to Bcast
//2. Send blocks to the master only after factorization
//3. ... And more optimization!
//4. Use only blas-lapack for matrix operations
