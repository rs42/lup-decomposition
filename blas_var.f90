subroutine lupfortran(A, rowpermut, n) bind(C)
    implicit none
    integer idamax
    external idamax
    external dswap
    external dger
    
    integer n, ip, i, k
    integer rowpermut(n)
    real*8 A(n, n)
    real*8 :: eps=1D-10
    do i = 1, n-1
        ip = idamax(n-i+1, A(i, i), n) + i - 1
        if (abs(A(i, ip)) < eps) then
            print *,"det A = 0 save me plz"
        end if
        
        if (i /= ip) then
            call dswap(n, A(1, i), 1, A(1, ip), 1)
            k = rowpermut(i)
            rowpermut(i) = rowpermut(ip)
            rowpermut(ip) = k
        end if
        call dscal(n-i, 1D0/A(i,i), A(i, i+1), n)
       
        call dger(n-i, n-i, -1D0, A(i+1, i), 1, A(i, i+1), n, A(i+1, i+1), n)
        !A(i+1: , i+1:) = A(i+1: , i+1:) - matmul(A(i+1:, i:i), A(i:i , i+1:))

    end do
end subroutine
