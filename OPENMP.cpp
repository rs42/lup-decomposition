#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <time.h>
#include <omp.h>
struct Matrix
{
    double *a;
    int m, n;
    int *row_permut, *col_permut;
    Matrix(const Matrix& z)
    : m(z.m), n(z.n)
    {
        a = new double [m*n];
        row_permut = new int [m];
        col_permut = new int [n];
        for (int i = 0; i < m; i++) 
            row_permut[i] = z.row_permut[i];
        for (int i = 0; i < n; i++)
            col_permut[i] = z.col_permut[i];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                (*this)(i,j) = z(i,j);
        }
    }
    double frobenius() const
    {
        double res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res += (*this)(i,j) * (*this)(i,j);
        }
        return sqrt(res);
    }
    bool operator==(const Matrix& x) const
    {
        if (m != x.m || n != x.n)
            return false;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if ((float)(*this)(i, j) != (float)x(i, j)) {
                    printf("%lf %lf\n", (*this)(i, j), x(i, j));
                    return false;
                }
            }
        }
        return true;
    }
    Matrix operator+(const Matrix& b) const
    {
        const Matrix &a = *this;
        Matrix res(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res(i, j) = a(i, j) + b(i, j);
        }
        return res;
    }
    Matrix operator-(const Matrix& b) const
    {
        const Matrix &a = *this;
        Matrix res(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res(i, j) = a(i, j) - b(i, j);
        }
        return res;
    }
    Matrix operator*(const Matrix& b) const
    {
        const Matrix &a = *this;
        Matrix res(m, b.n);
        for (int i = 0; i < res.m; i++) {
            for (int j = 0; j < res.n; j++) {
                res(i, j) = a(i, 0) * b(0, j);
                for (int k = 1; k < n; k++)
                    res(i, j) += a(i, k) * b(k, j);
            }
        }
        return res;
    }
    Matrix(int mm, int nn = 0)
    : m(mm), n(nn)
    {
        if (nn == 0)
            n = m;
        a = new double [m*n];
        row_permut = new int [m];
        col_permut = new int [n];
        for (int i = 0; i < m; i++) 
            row_permut[i] = i;
        for (int i = 0; i < n; i++)
            col_permut[i] = i;
    }
    ~Matrix()
    {
        delete [] a;
        delete [] row_permut;
        delete [] col_permut;
    }
    void print() const
    {
        for (int i = 0; i < m; i++) {
            printf("| ");
            for (int j = 0; j < n; j++)
                printf("%lf ", (*this)(i, j));
            printf("|\n");
        }
    }
    void random()
    {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                (*this)(i, j) = (double) rand() / RAND_MAX;
        }
    }
    inline void swap_rows(int i, int j)
    {
        double b;
        for (int k = 0; k < n; k++) {
            b = (*this)(i, k);
            (*this)(i, k) = (*this)(j, k);
            (*this)(j, k) = b;
        }
        int y = row_permut[i];
        row_permut[i] = row_permut[j];
        row_permut[j] = y;
    }

    inline double & operator()(int i, int j) const
    {
        return a[j+i*n];
    }
    void parallel_lup(double *X, double *Y, int L3_sz,int tnum);
    void slow_parallel_lup();
    void sequential_lup();
    void unlup();
};

void Matrix::unlup()
{
    Matrix& lu = *this;
    for (int i = n-1; i >= 0; i--) {
        for (int j = 0; j < i; j++)
            lu(i, i) += lu(i, j) * lu(j, i);
        for (int j = i-1; j >= 0; j--) {
            lu(i, j) *= lu(j, j);
            for (int k = 0; k < j; k++)
                lu(i, j) += lu(i, k) * lu(k, j);
        }
        for (int j = i-1; j >= 0; j--) {
            for (int k = 0; k < j; k++)
                lu(j, i) += lu(j, k) * lu(k, i);
        }
    }
    for (int i = 0; i < n; i++) {
        while (row_permut[i] != i)
            swap_rows(i, row_permut[i]);
    }
}

void Matrix::sequential_lup()
{
    if (m != n) {
        printf("Non-square matrix can't be decomposed\n");
        return;
    }
    Matrix &lu = *this;
    int max_row = 0;
    for (int i = 0; i < n; i++) {
        double max = 0;
        for (int k = i; k < n; k++) {
            if (fabs(lu(k, i)) > max) {
                max = fabs(lu(k, i));
                max_row = k;
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
            return;
        }
        if (i != max_row)
            swap_rows(i, max_row);

        for (int j = i + 1; j < n; j++) {
            lu(j, i) /= lu(i, i);
            for (int k = i + 1; k < n; k++)
                lu(j, k) -= lu(j, i) * lu(i, k);
        }
    }
}


void Matrix::parallel_lup(double *X, double *Y, int L3_sz, int tnum)
{
    double dmax[4];
    int imax[4];
    Matrix &lu=*this;
    int bs;
    double max;
    int max_row;
    int N = n;
    bs = N*N - L3_sz > 0 ? N - (int)sqrt(N*N-L3_sz) : N;
    for (int i = 0;;) {
        //
        // |_A_|_Y_|
        // | X | D |
        //
        //prepare well-aligned X
        for (int z = i + bs; z < n; z++) {
            for (int g = i; g < i+bs; g++)
                X[g - i + bs*(z-i-bs)] = lu(z, g);
        }
        // factorize A 
        for (int ii = i; ii < i+bs; ii++) {
        ////
        double coeff_ii;
        #pragma omp parallel shared(coeff_ii,X,lu,ii,i,bs)
        {////parallel
        int id = omp_get_thread_num();
        dmax[id] = 0;
        #pragma omp parallel for
        for (int k = ii; k < i+bs; k++) {
            if (fabs(lu(k,ii)) > dmax[id]) {
                dmax[id] = fabs(lu(k,ii));
                imax[id] = k;
            }
        }
        #pragma omp barrier
        #pragma omp single
        {//single
        max = 0;
        for (int f = 0; f < tnum; f++) {
            if (max < dmax[f]) {
                max = dmax[f];
                max_row = imax[f];
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
        }
        if (ii != max_row)
            swap_rows(ii, max_row);
        coeff_ii = lu(ii,ii);
        }//end_single
        
        #pragma omp for //schedule(dynamic,1)
        for (int j = ii + 1; j < i+bs; j++) {
            double coef = (lu(j, ii) /= coeff_ii);
            for (int k = ii + 1; k < n; k++)
                lu(j, k) -= coef * lu(ii, k);
        }
        #pragma omp for
        for (int j = 0; j < n-i-bs; j++) {
            double coef = (X[j*bs + ii - i] /= coeff_ii);
            for (int k = ii-i+1; k < bs; k++)
                X[j*bs + k] -= coef * lu(ii, k+i);
        }


        }////end_parallel
        ////
        }
        
        //now D -= X*Y
        
        //prepare Y

        int gg = n-i-bs;
        if (gg == 0) break;
        for (int z = 0; z < bs; z++) {
            for (int g = 0; g < gg; g++)
                Y[z + g*bs] = lu(i+z, i+bs+g);
        }
        int bdd = 32;// bdd = bs is a bit slower
        for (int a = 0; a < gg; a+=bdd) {
            int at = a+bdd;
            if (at > gg) at = gg;
            
            for (int b = 0; b < gg; b+=bdd) {
                int bt = b+bdd;
                if (bt > gg) bt = gg;

                #pragma omp parallel for shared(Y,X,lu,i,bs,bt,at) //collapse(2) //schedule(dynamic,1) 
                for (int j = a; j < at; j++) {
                    for (int k = b; k < bt; k++) {
                        double xx = 0;
                        for (int m = 0; m < bs; m++)
                          xx += X[m + j*bs] * Y[m + bs*k];
                        lu(j+i+bs, k+i+bs)-= xx;
                    }
                }
            }
        }

        for (int z = i + bs; z < n; z++) {
            for (int g = i; g < i+bs; g++)
                lu(z,g) = X[g - i + bs*(z-i-bs)];
        }
        i+=bs;
        if (i == n) break;
        N-=bs;
        bs = N*N - L3_sz > 0 ? N - (int)sqrt(N*N-L3_sz) : N;
        
    }
}

void Matrix::slow_parallel_lup()
{
    if (m != n) {
        printf("Non-square matrix can't be decomposed\n");
        return;
    }
    Matrix &lu = *this;
    int max_row = 0;
    for (int i = 0; i < n; i++) {
        double max = 0;
        for (int k = i; k < n; k++) {
            if (fabs(lu(k, i)) > max) {
                max = fabs(lu(k, i));
                max_row = k;
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
            return;
        }
        if (i != max_row)
            swap_rows(i, max_row);
        #pragma omp parallel for shared (lu, i) //schedule(dynamic,1)
        for (int j = i + 1; j < n; j++) {
            lu(j, i) /= lu(i, i);
            for (int k = i + 1; k < n; k++)
                lu(j, k) -= lu(j,i) * lu(i, k);
        }
    }
}

enum {numof_tests = 10, sz = 4};
int main(void)
{
    srand(time(0));
    const int mtrx[sz] = {256, 512, 1024, 2048};//4096};
    double res[sz];
    double buf;
    double error; //relative error based on frobenius norm
    
    const int L3_sz = 524288; //6Mib approx 2^19 doubles
    const int max_t = 4;
    double *X = new double[L3_sz];
    double *Y = new double[L3_sz];

    double su[max_t][sz];
    int tnum; 
    for (int tt = 0; tt < max_t; tt++) {
    ///////////
        omp_set_num_threads(tt+1);
        printf("%d threads\n", (tnum = omp_get_max_threads()));
    
    for (int i = 0; i < sz; i++) {
        buf = 0;
        error = 0;
        for (int k = 0 ; k < numof_tests; k++) {
            Matrix A(mtrx[i]);
            A.random();
            Matrix LUP(A);
            double t = omp_get_wtime();
            LUP.parallel_lup(X, Y, L3_sz, tnum);
            t = omp_get_wtime() - t;
            buf += t;
            
            //counting error
            //LUP.unlup();
            //LUP.print();
            //printf("\n\n");
            //error += (A - LUP).frobenius() / A.frobenius();
        }
        res[i] = buf / numof_tests;
        error /= numof_tests;
        printf("%d | err: %e | %lf sec \n",
                mtrx[i], error, res[i]);
    }
    printf("Complete. Calculating exponent...\n");
    for (int i = 0; i < sz - 1; i++) {
        printf("%d - %d : %lf\n", mtrx[i], mtrx[i+1],
        log2( (double)res[i+1]/res[i] ));
    }
    for (int i = 0; i < sz; i++)
        su[tt][i]=res[i];
    //////////////
    }
    printf("****SPEEDUP***\n   +1  +2  +3 ...\n");
    for (int i = 0; i < sz; i++) {
        printf("%d: ", mtrx[i]);
        for (int k = 1; k < max_t; k++)
            printf(" %lf", su[0][i]/su[k][i]);
        printf("\n");
    }
    delete [] X;
    delete [] Y;
    return 0;
}

/*
int main(void)
{
    srand(time(0));
    const int mtrx[sz] = {256, 512, 1024, 2048};
    clock_t res[sz];
    long long int buf;
    double error; //relative error based on frobenius norm
    for (int i = 0; i < sz; i++) {
        buf = 0;
        error = 0;
        for (int k = 0 ; k < numof_tests; k++) {
            Matrix A(mtrx[i]);
            A.random();
            Matrix LUP(A);
            //A.print();
            //printf("\n\n");
            clock_t t = clock();
            #ifdef FORTRAN
            LUP.lup_decomp_fortran();
            #else
            LUP.lup_decomp_mod();
            #endif
            t = clock() - t;
            buf += t;
            
            //counting error
            #ifdef FORTRAN
            LUP.unlup_fortran();
            #else
            LUP.unlup();
            #endif
            //LUP.print();
            //printf("\n\n");
            error += (A - LUP).frobenius() / A.frobenius();
        }
        res[i] = buf / numof_tests;
        error /= numof_tests;
        printf("%d | err: %e | %f sec \n",
                mtrx[i], error, (float)res[i] / CLOCKS_PER_SEC);
    }
    printf("Complete. Calculating exponent...\n");
    for (int i = 0; i < sz - 1; i++) {
        printf("%d - %d : %lf\n", mtrx[i], mtrx[i+1],
        log2( (double)res[i+1]/res[i] ));
    }
    return 0;
}
*/
