#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <time.h>

extern "C"
void lupfortran
(double *matrix, int *row_permut, int *size);

#ifndef FAST
//classes for exceptions
class WrongRowsSwap
{};
class OutOfMatrix
{};
class BadMult
{};
class BadAdd
{};
class BadSubtract
{};
#endif

struct Matrix
{
    double **rows;
    double *a;

    int m, n;
    int *row_permut, *col_permut;
    Matrix(const Matrix& z)
    : m(z.m), n(z.n)
    {
        rows = new double* [m];
        a = new double [m*n];
        row_permut = new int [m];
        col_permut = new int [n];
        for (int i = 0; i < m; i++) {
            rows[i] = a + i*n;
            row_permut[i] = z.row_permut[i];
        }
        for (int i = 0; i < n; i++)
            col_permut[i] = z.col_permut[i];
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                rows[i][j] = z.rows[i][j];
        }
    }
    double frobenius() const
    {
        double res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res += rows[i][j] * rows[i][j];
        }
        return sqrt(res);
    }
    bool operator==(const Matrix& x) const
    {
        if (m != x.m || n != x.n)
            return false;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if ((float)(*this)(i, j) != (float)x(i, j)) {
                    printf("%lf %lf\n", (*this)(i, j), x(i, j));
                    return false;
                }
            }
        }
        return true;
    }
    Matrix operator+(const Matrix& b) const
    {
        #ifndef FAST
        if (n != b.n || m != b.m)
            throw BadAdd();
        #endif
        const Matrix &a = *this;
        Matrix res(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res(i, j) = a(i, j) + b(i, j);
        }
        return res;
    }
    Matrix operator-(const Matrix& b) const
    {
        #ifndef FAST
        if (n != b.n || m != b.m)
            throw BadSubtract();
        #endif
        const Matrix &a = *this;
        Matrix res(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                res(i, j) = a(i, j) - b(i, j);
        }
        return res;
    }
    Matrix operator*(const Matrix& b) const
    {
        #ifndef FAST
        if (n != b.m)
            throw BadMult();
        #endif
        const Matrix &a = *this;
        Matrix res(m, b.n);
        for (int i = 0; i < res.m; i++) {
            for (int j = 0; j < res.n; j++) {
                res(i, j) = a(i, 0) * b(0, j);
                for (int k = 1; k < n; k++)
                    res(i, j) += a(i, k) * b(k, j);
            }
        }
        return res;
    }
    Matrix(int mm, int nn = 0)
    : m(mm), n(nn)
    {
        if (nn == 0)
            n = m;
        rows = new double* [m];
        a = new double [m*n];
        row_permut = new int [m];
        col_permut = new int [n];
        for (int i = 0; i < m; i++) {
            rows[i] = a + i*n;
            row_permut[i] = i;
        }
        for (int i = 0; i < n; i++)
            col_permut[i] = i;
    }
    ~Matrix()
    {
        delete [] rows;
        delete [] a;
        delete [] row_permut;
        delete [] col_permut;
    }
    void print() const
    {
        for (int i = 0; i < m; i++) {
            printf("| ");
            for (int j = 0; j < n; j++)
                printf("%lf ", (*this)(i, j));
            printf("|\n");
        }
    }
    void random()
    {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                (*this)(i, j) = (double) rand() / RAND_MAX;
        }
    }
    void swap_rows(int i, int j)
    {
        #ifdef FAST
        //double *x = rows[i];
        //rows[i] = rows[j];
        //rows[j] = x;
        double b;
        for (int k = 0; k < n; k++) {
            b = (*this)(i, k);
            (*this)(i, k) = (*this)(j, k);
            (*this)(j, k) = b;
        }
        int y = row_permut[i];
        row_permut[i] = row_permut[j];
        row_permut[j] = y;
        #else
        if (i < m && j < m && i != j) {
            double *x = rows[i];
            rows[i] = rows[j];
            rows[j] = x;
            int y = row_permut[i];
            row_permut[i] = row_permut[j];
            row_permut[j] = y;
        } else {
            throw WrongRowsSwap();
        }
        #endif
    }

    double & operator()(int i, int j) const
    {
        #ifdef FAST
        return rows[i][j];
        #else
        if (i < m && j < n)
            return rows[i][j];
        else
            throw OutOfMatrix();
        #endif
    }
    void lup_decomp();
    void lup_decomp_fortran();
    void unlup();
    void unlup_fortran();
};

void Matrix::unlup_fortran()
{
    Matrix& lu = *this;
    for (int i = n-1; i >= 0; i--) {
        for (int j = 0; j < i; j++)
            lu(i, i) += lu(i, j) * lu(j, i);
        for (int j = i-1; j >= 0; j--) {
            lu(i, j) *= lu(j, j);
            for (int k = 0; k < j; k++)
                lu(i, j) += lu(i, k) * lu(k, j);
        }
        for (int j = i-1; j >= 0; j--) {
            for (int k = 0; k < j; k++)
                lu(j, i) += lu(j, k) * lu(k, i);
        }
    }
    //print();
    //printf("\n\n");
    //printf("READY\n");
    #ifdef IPIV
    for (int i = n-1; i >= 0; i--) {
        //printf("%d : %d\n", i, row_permut[i]);
        double b;
        if (row_permut[i] == i) continue;
        for (int k = 0, j = row_permut[i]; k < n; k++) {
            b = (*this)(i, k);
            (*this)(i, k) = (*this)(j, k);
            (*this)(j, k) = b;
        }
        //swap_rows(i, row_permut[i]);
    }
    //printf("\n\n");
    #else
    for (int i = 0; i < n; i++) {
        while (row_permut[i] != i)
            swap_rows(i, row_permut[i]);
    }
    #endif
}

void Matrix::lup_decomp_fortran()
{
    lupfortran(a, row_permut, (int*)&n);
}

void Matrix::unlup()
{
    //be sure it's lup before calling this method
    //tip:
    //
    //     |L' | | |U' | |
    //LU = |_ _|_| |_ _|_|
    //     |   |1| |   |u|
    //
    /*Matrix& lu = *this;
    for (int i = n-1; i >= 0; i--) {
        for (int j = 0; j < i; j++)
            lu(i, i) += lu(i, j) * lu(j, i);
        for (int j = i-1; j >= 0; j--) {
            lu(i, j) *= lu(j, j);
            for (int k = 0; k < j; k++)
                lu(i, j) += lu(i, k) * lu(k, j); 
        }
        for (int j = i-1; j >= 0; j--) {
            for (int k = 0; k < j; k++)
                lu(j, i) += lu(j, k) * lu(k, i);
        }
    }
    for (unsigned i = 0; i < n; i++) {
        rows[i] = a + i*n;
        row_permut[i] = i;
    }*/
    unlup_fortran();
}
void Matrix::lup_decomp()
{
    if (m != n) {
        printf("Non-square matrix can't be decomposed\n");
        return;
    }
    Matrix &lu = *this;
    int max_raw = 0;
    for (int i = 0; i < n; i++) {
        double max = 0;
        for (int k = i; k < n; k++) {
            if (fabs(lu(k, i)) > max) {
                max = fabs(lu(k, i));
                max_raw = k;
            }
        }
        if (max == 0) {
            printf("det A = 0\n");
            return;
        }
        if (i != max_raw)
            swap_rows(i, max_raw);

        for (int j = i + 1; j < n; j++) {
            lu(j, i) /= lu(i, i);
            for (int k = i + 1; k < n; k++)
                lu(j, k) -= lu(j, i) * lu(i, k);
        }
    }
}

enum {numof_tests = 10, sz = 4};

int main(void)
{
    srand(time(0));
    const int mtrx[sz] = {256, 512, 1024, 2048};
    clock_t res[sz];
    long long int buf;
    double error; //relative error based on frobenius norm
    for (int i = 0; i < sz; i++) {
        buf = 0;
        error = 0;
        for (int k = 0 ; k < numof_tests; k++) {
            Matrix A(mtrx[i]);
            A.random();
            Matrix LUP(A);
            //A.print();
            //printf("\n\n");
            clock_t t = clock();
            #ifdef FORTRAN
            LUP.lup_decomp_fortran();
            #else
            LUP.lup_decomp();
            #endif
            t = clock() - t;
            buf += t;
            
            //counting error
            #ifdef FORTRAN
            LUP.unlup_fortran();
            #else
            LUP.unlup();
            #endif
            //LUP.print();
            //printf("\n\n");
            error += (A - LUP).frobenius() / A.frobenius();
        }
        res[i] = buf / numof_tests;
        error /= numof_tests;
        printf("%d | err: %e | %f sec \n",
                mtrx[i], error, (float)res[i] / CLOCKS_PER_SEC);
    }
    printf("Complete. Calculating exponent...\n");
    for (int i = 0; i < sz - 1; i++) {
        printf("%d - %d : %lf\n", mtrx[i], mtrx[i+1],
        log2( (double)res[i+1]/res[i] ));
    }
    return 0;
}
