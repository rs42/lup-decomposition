subroutine lupfortran(A, rowpermut, n) bind(C)
    implicit none
    external dgetrf
    integer n, info, i
    real*8 A(n,n)
    integer rowpermut(n)
    A = transpose(A)
    call dgetrf(n, n, A, n, rowpermut, info)
    do i = 1, n
       rowpermut(i) = rowpermut(i)-1
    end do
    if (info /= 0) then
        print *,"something gone wrong"
    end if
    A = transpose(A)
end subroutine
