subroutine lupfortran(A, rowpermut, n) bind(C)
    implicit none
    integer n, i, j, k, ip
    integer rowpermut(n)
    real*8 A(n, n), pivot, swap(n)
    real*8 :: eps = 1D-10
    do i = 1, n
        pivot = 0
        do k = i, n
            if (abs(A(i, k)) > pivot) then
                pivot = abs(A(i, k))
                ip = k
            end if
        end do
       
        if (pivot < eps) then
            print *,"det A = 0 save me plz"
        end if
        
        if (i /= ip) then
            swap = A(:, i)
            A(:, i) = A(:, ip)
            A(:, ip) = swap
            k = rowpermut(i)
            rowpermut(i) = rowpermut(ip)
            rowpermut(ip) = k
        end if
        
        do j = i + 1, n
            A(i, j) = A(i, j) / A(i, i)
            A(i+1:, j) = A(i+1:, j) - A(i, j) * A(i+1:, i)
        end do
    end do
end subroutine
